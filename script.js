function createNewUser(){

    let newUser = {
        firstname: '',
        lastname: '',
        birthday: '',
        setFirstName(newName) {
            Object.defineProperty(newUser, 'firstname', {value: this.firstname = newName});
        },
        setLastName(newLName) {
            Object.defineProperty(newUser, 'lastname', {value: this.lastname = newLName});
        },
        getLogin() {
            let login = this.firstname[0].toLowerCase() + this.lastname.toLowerCase();
            return  'login: ' + 
            login
        },
        getAge() {
            let birthday = new Date(this.birthday);
            let today = new Date();
            let userAge = today.getFullYear() - birthday.getFullYear();

          return userAge + ' years old.'
        },
        getPassword() {
            let birthday = new Date(this.birthday);
            let password = this.firstname[0].toUpperCase() + this.lastname.toLowerCase() + birthday.getFullYear()

            return 'password: ' + password
        },

    }
    Object.defineProperty(newUser, 'firstname', { value: prompt('Firstname: '), writable: false, configurable: true });
    Object.defineProperty(newUser, 'lastname', { value: prompt('Lastname: '), writable: false, configurable: true }); 
    Object.defineProperty(newUser, 'birthday', { value: prompt(`Birthday 'dd.mm.yyyy': `), writable: false, configurable: true }); 
	return newUser;

}


// Test
let user = createNewUser();


console.log(user);
console.log(user.getAge());
console.log(user.getLogin());
console.log(user.getPassword());





